var assert = require('assert');
var webdriver = require('selenium-webdriver');
var test = require('selenium-webdriver/testing');
var Page = require('./pageobject.js');
var driver;
var loginSuccess = true;
var constants = require('./constants.js');
 
const mochaTimeOut = 30000; //ms
 
test.describe('Test Result:', function() {
    this.timeout(mochaTimeOut);
    
    test.before(function() {
        this.timeout(mochaTimeOut);
        driver = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build();
        page = new Page(driver);
        page.visit(constants.url);
    });

    test.beforeEach(function() {
        if (!loginSuccess)
            this.skip();
    });

    test.it('can login', function() {
        page.login().then(function(result) {
            assert.ok(result !== 1, 'Invalid username or password');
            assert.ok(result !== 2, 'Server not available');
            if (result === 0)
                loginSuccess = true;
            else
                loginSuccess = false;
        });
    });

    test.it('can enter brochure', function() {
        page.enterBrochure().then(function(result) {
            assert.equal(result, true, 'can\'t enter brochure');
        });
    });

    test.it('can load more entries', function() {
        page.loadMoreEntries().then(function(result) {
            assert.equal(result, true, 'can\'t load more entries');
        });;
    });

    test.it('can open entry', function() {
        page.openEntry().then(function(result) {
            assert.equal(result, true, 'can\'t open entry');
        });
    });

    test.it('can toggle entry sidebar', function() {
        page.toggleEntrySidebar().then(function(result) {
            assert.equal(result, true, 'can\'t toggle entry sidebar');
        });
    });

    test.it('can quit entry', function() {
        page.quitEntry().then(function(result) {
            assert.equal(result, true, 'can\'t quit entry');
        });
    });

    test.it('can change entry order', function() {
        page.changeEntryOrder().then(function(result) {
            assert.equal(result, true, 'can\'t change entry order');
        });
    });

    test.it('can change entry layout', function() {
        page.changeEntryLayout().then(function(result) {
            assert.equal(result, true, 'can\'t change entry layout');
        });
    });

    test.it('can filter entries', function() {
        page.filterEntries().then(function(result) {
            assert.equal(result, true, 'can\'t filter entries');
        });
    });

    test.afterEach(function() {
	    //driver.manage().deleteAllCookies();
	});
	 
	test.after(function() {
        driver.sleep(2000);
	    driver.quit();
	});
});