var assert = require('assert');
var webdriver = require('selenium-webdriver');
var test = require('selenium-webdriver/testing');
var Page = require('./pageobject.js');
var driver;
var loginSuccess = true;
var constants = require('./constants.js');
 
const mochaTimeOut = 30000; //ms
 
test.describe('Test Result:', function() {
    this.timeout(mochaTimeOut);
    
    test.before(function() {
        this.timeout(mochaTimeOut);
        driver = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build();
        page = new Page(driver);
        page.visit(constants.url);
    });

    test.beforeEach(function() {
        if (!loginSuccess)
            this.skip();
    });

    test.it('can login', function() {
        page.login().then(function(result) {
            assert.ok(result !== 1, 'Invalid username or password');
            assert.ok(result !== 2, 'Server not available');
            if (result === 0)
                loginSuccess = true;
            else
                loginSuccess = false;
        });
    });

    test.it('can refresh', function() {
        page.refresh().then(function(result) {
            assert.equal(result, true, 'can\'t refresh');
        });
    });

    test.it('can select connection', function() {
        page.selectConnection('#connection-2522', '#brochure-15034').then(function(result) {
            assert.equal(result, true, 'can\'t select connection');
        });
    });

    test.it('can enter brochure', function() {
        page.enterBrochure('#connection-2522', '#brochure-15034').then(function(result) {
            assert.equal(result, true, 'can\'t enter brochure');
        });
    });

    test.it('can quit brochure', function() {
        page.quitBrochure().then(function(result) {
            assert.equal(result, true, 'can\'t quit brochure');
        });
    });

    test.it('can select event', function() {
        page.selectEvent().then(function(result) {
            assert.equal(result, true, 'can\'t select event');
        });
    });

    test.it('can view event detail', function() {
        page.viewEventDetail().then(function(result) {
            assert.equal(result, true, 'can\'t view event detail');
        });;
    });

    test.it('can log out', function() {
        page.logout().then(function(result) {
            assert.equal(result, true, 'can\'t log out');
        });
    });

    test.afterEach(function() {
	    //driver.manage().deleteAllCookies();
	});
	 
	test.after(function() {
        driver.sleep(2000);
	    driver.quit();
	});
});