var constants = {
	waitTime: 5000,
	url: 'http://dev-my.interact.technology/interact/#/login',
	registerUrl: 'http://dev-my.interact.technology/utilities/register', 
	email: 'midautotest@yopmail.com',
	password: 'Password1',
	loginPage: {
		email: 'email',
		password: 'password',
		loginBtn: '#submitBtn',
		forgotPassword: '#forgotPassword',
		systemRequirements: '#systemRequirements'
	},
	header: {
		connectionsIcon: '#connectionsIcon',
		eventsIcon: '#eventsIcon',
		refreshBtn: '#refreshIconContainer',
		menuIcon: '#menuIcon',
		logoutBtn: '#logOut',
		logoutConfirmBtn: '#confirmlogout',
		changePasswordBtn: '#changePasswordMenuButton',
		quitBrochure: '#quitBrochure',
		uploadButton: '#uploadBtn'
	},
	connectionsPage: {
		sidebar: '#sidebar-wrapper',
		connection: '#connection-2',
		brochure: '#brochure-0',		
		event: '#event-1',
		eventHeader: '#event-header',
		eventContent: '.event-content'
	},
	brochurePage: {
		brochureIframe: '.brochureIframe'
	},
	smartLibrary: {
		entry: '#smartEntry-0',
		secondPageEntry: '#smartEntry-10',
		closeSidebar: '#toggleMetadata',
		openSidebar: '#toggleMetadataCollapsed',
		backToEntries: '#backToEntries',
		searchBar: '.entry-search-keywords',
		viewBy: '#viewBy',
		viewByNameAsc: '#viewByNameAsc',
		viewByNameDesc: '#viewByNameDesc',
		viewByDateAsc: '#viewByDateAsc',
		viewByDateDesc: '#viewByDateDesc',
		layoutButton: '#layoutButton',
		listLayout: '.entry-thumbnail-listLayout',
		filterButton: '#filterButton',
		clearFilter: '#clearFilter',
		filter: '#filter-0'
	},
	smlbUploader: {
		uploadForm: '#uploadForm',
		deleteButton: '#deleteButton',
		selectAllButton: '#selectAllButton',
		existingTag: '#existingTag-0'
	},
	registerPage: {
		registerButton: '#submit-button',
		termsButton: '#termsOfService',
		privacyButton: '#privacyPolicy',
		loginButton: '#login-button'
	},
	files: {
		file1: '/Users/xiaoli/Desktop/goodtimes.jpg'
	}
}

module.exports = constants;