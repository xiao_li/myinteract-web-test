var constants = require('./constants.js');

var webdriver = require('selenium-webdriver'),
	By = webdriver.By,
	until = webdriver.until,
	Key = webdriver.Key;

page = function page(driver) {
	this.driver = driver;
	this.selector = webdriver.By.name('btnK');
}

page.prototype.visit = function(url) {
	this.driver.get(url);
	return webdriver.promise.fulfilled(true);
}

/* MID main */

page.prototype.login = function() {
	this.driver.findElement(By.name(constants.loginPage.email)).sendKeys(constants.email);
	this.driver.findElement(By.name(constants.loginPage.password)).sendKeys(constants.password);
	this.driver.findElement(By.css(constants.loginPage.loginBtn)).click();
	var d = webdriver.promise.defer();
	var self = this;
	this.driver.wait(until.elementLocated(By.css(constants.connectionsPage.sidebar)), constants.waitTime).then(() => {
		d.fulfill(0);
	}).catch(() => {
		self.driver.findElement(By.css('.errors')).then(() => {
			d.fulfill(1);
		}).catch(() => {
			d.fulfill(2);
		});
	});
	return d.promise;
}

page.prototype.refresh = function() {
	this.driver.findElement(By.css(constants.header.refreshBtn)).click();
	return returnPromise('#connection-2522', constants.waitTime);
}

page.prototype.selectConnection = function(connectionId, brochureId) {
	this.driver.wait(until.elementLocated(By.css(connectionId)), constants.waitTime);
	this.driver.findElement(By.css(connectionId)).click();
	return returnPromise(brochureId, constants.waitTime);
}

page.prototype.enterBrochure = function(connectionId, brochureId) {
	this.driver.wait(until.elementLocated(By.css(connectionId)), constants.waitTime);
	this.driver.findElement(By.css(connectionId)).click();
	this.driver.sleep(500);
	this.driver.wait(until.elementLocated(By.css(brochureId)), constants.waitTime);
	this.driver.findElement(By.css(brochureId)).click();
	return returnPromise(constants.header.quitBrochure, 10000);
}

page.prototype.quitBrochure = function() {
	this.driver.switchTo().defaultContent();
	this.driver.findElement(By.css(constants.header.quitBrochure)).click();
	return returnPromise(constants.connectionsPage.sidebar, constants.waitTime);
}

page.prototype.selectEvent = function() {
	this.driver.findElement(By.css(constants.header.eventsIcon)).click();
	this.driver.wait(until.elementLocated(By.css(constants.connectionsPage.event)), constants.waitTime);
	this.driver.findElement(By.css(constants.connectionsPage.event)).click();
	return returnPromise(constants.connectionsPage.eventHeader, constants.waitTime);
}

page.prototype.viewEventDetail = function() {
	this.driver.wait(until.elementLocated(By.css(constants.connectionsPage.eventHeader)), constants.waitTime);
	this.driver.findElement(By.css(constants.connectionsPage.eventHeader)).click();
	var d = webdriver.promise.defer();
	this.driver.wait(until.elementIsVisible(this.driver.findElement(By.css(constants.connectionsPage.eventContent))), constants.waitTime).then(() => {
	}).catch(() => {
		d.fulfill(false);
	});
	this.driver.findElement(By.css(constants.connectionsPage.eventHeader)).click();
	this.driver.wait(until.elementIsNotVisible(this.driver.findElement(By.css(constants.connectionsPage.eventContent))), constants.waitTime).then(() => {
		d.fulfill(true);
	}).catch(() => {
		d.fulfill(false);
	});
	return d.promise;
}

page.prototype.logout = function() {
	this.driver.findElement(By.css(constants.header.menuIcon)).click();
	this.driver.sleep(500);
	this.driver.findElement(By.css(constants.header.logoutBtn)).click();
	this.driver.sleep(500);
	this.driver.findElement(By.css(constants.header.logoutConfirmBtn)).click();
	return returnPromise(constants.loginPage.loginBtn, constants.waitTime);
}

/* end MID main */

/* smart library */

page.prototype.loadMoreEntries = function() {
	this.driver.switchTo().frame("brochureIframe");
	this.driver.wait(until.elementLocated(By.css(constants.smartLibrary.entry)), 10000);
	this.driver.executeScript("document.getElementsByClassName('entry-container')[0].scrollTop = 999;");
	return returnPromise(constants.smartLibrary.secondPageEntry, constants.waitTime);
}

page.prototype.openEntry = function() {
	this.driver.findElement(By.css(constants.smartLibrary.entry)).click();
	return returnPromise(constants.smartLibrary.backToEntries, constants.waitTime);
}

page.prototype.toggleEntrySidebar = function() {
	this.driver.findElement(By.css(constants.smartLibrary.closeSidebar)).click();
	var d = webdriver.promise.defer();
	this.driver.wait(until.elementIsVisible(this.driver.findElement(By.css(constants.smartLibrary.openSidebar))), constants.waitTime).then(() => {
	}).catch(() => {
		d.fulfill(false);
	});
	this.driver.sleep(500);
	this.driver.findElement(By.css(constants.smartLibrary.openSidebar)).click();
	this.driver.wait(until.elementIsVisible(this.driver.findElement(By.css(constants.smartLibrary.backToEntries))), constants.waitTime).then(() => {
		d.fulfill(true);
	}).catch(() => {
		d.fulfill(false);
	});
	return d.promise;
}

page.prototype.quitEntry = function() {
	this.driver.sleep(500);
	this.driver.findElement(By.css(constants.smartLibrary.backToEntries)).click();
	return returnPromise(constants.smartLibrary.searchBar, constants.waitTime);
}

page.prototype.changeEntryOrder = function() {
	this.driver.findElement(By.css(constants.smartLibrary.viewBy)).click();
	//this.driver.wait(until.elementLocated(By.css(constants.smartLibrary.viewByNameAsc)), constants.waitTime);
	this.driver.findElement(By.css(constants.smartLibrary.viewByNameAsc)).click();
	this.driver.sleep(1000);

	// TODO: write test after creating a test smartlibrary with fixed asset

	var d = webdriver.promise.defer();
	d.fulfill(true);
	return d.promise;
}

page.prototype.changeEntryLayout = function() {
	var d = webdriver.promise.defer();
	this.driver.findElement(By.css(constants.smartLibrary.layoutButton)).click();
	this.driver.sleep(500);
	var self = this;
	this.driver.findElements(By.css(constants.smartLibrary.listLayout)).then(function(elements) {
		if (elements.length) {
			self.driver.findElement(By.css(constants.smartLibrary.layoutButton)).click();
			self.driver.sleep(500);
			self.driver.findElements(By.css(constants.smartLibrary.listLayout)).then(function(elements2) { 
				if (elements2.length)
					d.fulfill(false);
				else
					d.fulfill(true);
			})
		} else {
			d.fulfill(false);
		}
	});
	return d.promise;
}

page.prototype.filterEntries = function() {
	var d = webdriver.promise.defer();
	this.driver.findElement(By.css(constants.smartLibrary.filterButton)).click();

	// TODO: write test after creating a test smartlibrary with fixed asset

	d.fulfill(true);
	return d.promise;
}
/* end smart library */

/* MID register */

page.prototype.showErrorMessage = function() {
	this.driver.wait(until.elementLocated(By.css(constants.registerPage.registerButton)), constants.waitTime);
	this.driver.findElement(By.css(constants.registerPage.registerButton)).click();
	var d = webdriver.promise.defer();

	this.driver.findElement(By.css('#error-firstName')).getText().then(function(text) {if (text === '') d.fulfill('failed to check empty firstName');});
	this.driver.findElement(By.css('#error-lastName')).getText().then(function(text) {if (text === '') d.fulfill('failed to check empty firstName');});
	this.driver.findElement(By.css('#error-email')).getText().then(function(text) {if (text === '') d.fulfill('failed to check empty email');});
	this.driver.findElement(By.css('#error-password')).getText().then(function(text) {if (text === '') d.fulfill('failed to check empty password');});
	this.driver.findElement(By.css('#error-passwordConfirm')).getText().then(function(text) {if (text === '') d.fulfill('failed to check empty password confirm');});
	this.driver.findElement(By.css('#error-recaptcha')).getText().then(function(text) {if (text === '') d.fulfill('failed to check empty recaptcha');});
	
	this.driver.findElement(By.css('#firstName')).sendKeys('firstName');
	this.driver.findElement(By.css('#lastName')).sendKeys('lastName');
	this.driver.findElement(By.css('#email')).sendKeys('firstName@yopmail.com');
	this.driver.findElement(By.css(constants.registerPage.registerButton)).click();

	this.driver.findElement(By.css('#error-firstName')).getText().then(function(text) {if (text !== '') d.fulfill('shouldn\'t show error when firstName is not empty');});
	this.driver.findElement(By.css('#error-lastName')).getText().then(function(text) {if (text !== '') d.fulfill('shouldn\'t show error when lastName is not empty');});
	this.driver.findElement(By.css('#error-email')).getText().then(function(text) {if (text !== '') d.fulfill('shouldn\'t show error when email is not empty');});
	
	this.driver.findElement(By.css('#password')).sendKeys('passwor');
	this.driver.findElement(By.css('#error-password')).getText().then(function(text) {if (text === '') d.fulfill('failed to check password shorter than 8 characters');});
	this.driver.findElement(By.css('#password')).clear();
	this.driver.findElement(By.css('#password')).sendKeys('password');
	this.driver.findElement(By.css('#error-password')).getText().then(function(text) {if (text === '') d.fulfill('failed to check password with no capital letter');});
	this.driver.findElement(By.css('#password')).clear();
	this.driver.findElement(By.css('#password')).sendKeys('Password');
	this.driver.findElement(By.css('#error-password')).getText().then(function(text) {if (text === '') d.fulfill('failed to check password with no number');});
	this.driver.findElement(By.css('#password')).clear();
	this.driver.findElement(By.css('#password')).sendKeys('Password1');
	this.driver.findElement(By.css('#error-password')).getText().then(function(text) {if (text !== '') d.fulfill('shouldn\'t show error for a valid password');});
	this.driver.findElement(By.css('#passwordConfirm')).sendKeys('Password2');
	this.driver.findElement(By.css('#error-passwordConfirm')).getText().then(function(text) {if (text === '') d.fulfill('failed to check 2 passwords are not identical');});
	this.driver.findElement(By.css('#passwordConfirm')).clear();
	this.driver.findElement(By.css('#passwordConfirm')).sendKeys('Password1');
	this.driver.findElement(By.css(constants.registerPage.registerButton)).click();
	this.driver.findElement(By.css('#error-passwordConfirm')).getText().then(function(text) {if (text !== '') d.fulfill('shouldn\'t show error when passwords are identical');});
	
	return d.promise;
}

page.prototype.viewTermsAndPolicy = function() {
	var Keys = require('selenium-webdriver').Key;
	var d = webdriver.promise.defer();
	this.driver.findElement(By.css(constants.registerPage.termsButton)).click();
	
	this.driver.getAllWindowHandles().then((result) => {
		console.log(result);
		this.driver.switchTo().window(result[1]);
		this.driver.getCurrentUrl().then((termsUrl) => {
			if (termsUrl !== 'https://s3-us-west-1.amazonaws.com/interact.technology/legal/internet-services/au/EULA.html')
				d.fulfill('terms of service');
			this.driver.switchTo().window(result[0]);
			this.driver.findElement(By.css(constants.registerPage.privacyButton)).click();
			this.driver.getAllWindowHandles().then((tabs) => {
				this.driver.switchTo().window(tabs[2]);
				this.driver.getCurrentUrl().then((privacyUrl) => {
					if (privacyUrl !== 'https://s3-us-west-1.amazonaws.com/interact.technology/legal/internet-services/au/privacy.html')
						d.fulfill('privacy policy');
					this.driver.switchTo().window(result[0]);
				});
			});
		});
	});

	return d.promise;
}

page.prototype.goToLoginPage = function() {
	var d = webdriver.promise.defer();
	this.driver.findElement(By.css(constants.registerPage.loginButton)).click();
	this.driver.wait(until.elementLocated(By.css(constants.loginPage.loginBtn)), constants.waitTime).then(() => {
		d.fulfill(true);
	}).catch(() => {
		d.fulfill(false);
	});
	return d.promise;
}

/* end MID register */

/* MID uploader */

page.prototype.openUploader = function() {
	var d = webdriver.promise.defer();
	this.driver.wait(until.elementLocated(By.css(constants.header.uploadButton)), constants.waitTime).then(() => {
		this.driver.findElement(By.css(constants.header.uploadButton)).click();
		this.driver.switchTo().frame("uploaderIframe");
		this.driver.wait(until.elementLocated(By.css(constants.smlbUploader.uploadForm)), constants.waitTime).then(() => {
			d.fulfill(true);
		}).catch(() => {
			d.fulfill(false);
		});
	}).catch(() => { d.fulfill(false); });
	
	return d.promise;
}

page.prototype.deleteFile = function() {
	this.driver.findElement(By.css(constants.smlbUploader.deleteButton)).click();
	var d = webdriver.promise.defer();
	this.driver.wait(until.elementsLocated(By.css('.qq-upload-success')), 1000).then(() => {
		d.fulfill(false);
	}).catch(() => {
		d.fulfill(true);
	});
	return d.promise;
}

page.prototype.editTag = function() {
	var uploadResult = this.uploadFile();
	if (uploadResult) {
		var d = webdriver.promise.defer();
		this.driver.findElement(By.css(constants.smlbUploader.existingTag)).click();
		this.driver.wait(until.elementsLocated(By.css('.tag-item')), 1000).then(() => {
			this.driver.findElements(By.css('.remove-button')).then((removeButtons)=> {
				removeButtons[0].click();
				this.driver.wait(until.elementsLocated(By.css('.tag-item')), 1000).then(() => {
					d.fulfill(`can't delete tag`);
				}).catch(() => {
					this.driver.findElement(By.xpath("//*[contains(@class,'tags')]/input")).then((element) => {
						element.sendKeys('12345', Key.ENTER);
						this.driver.wait(until.elementsLocated(By.css('.tag-item')), 1000).then(() => {
							element.sendKeys('ab');
							this.driver.sleep(500);
							this.driver.findElements(By.css('.suggestion-item')).then((elements) => {
								elements[0].click();
								this.driver.wait(until.elementsLocated(By.css('.tag-item')), 1000).then((tagItems) => {
									if (tagItems.length == 2)
										d.fulfill(true);
									else
										d.fulfill(`can't add tag from dictionary`);
								});
							}).catch(() => {	
								d.fulfill(`can't select from dictionary`);
							});
						}).catch(() => {
							d.fulfill(`can't add tag`);
						});
					}).catch(() => {
						d.fulfill(`can't find tag input element`);
					});
				});
			}).catch(() => {
				d.fulfill(`can't find delete tag button`);
			});
		}).catch(() => {
			d.fulfill(`can't select existing tag`);
		});
		return d.promise;
	}
}

page.prototype.uploadFile = function() {
	this.driver.findElement(By.name('smartFile')).then((element) => { 
		element.sendKeys(constants.files.file1); 
	});
	var d = webdriver.promise.defer();
	this.driver.wait(until.elementsLocated(By.css('.qq-upload-success')), constants.waitTime).then(() => {
		d.fulfill(true);
	}).catch(() => {
		d.fulfill(false);
	});
	return d.promise;
}

/* end MID uploader */

function returnPromise(element, waitTime) {
	var d = webdriver.promise.defer();
	page.driver.wait(until.elementLocated(By.css(element)), waitTime).then(() => {
		d.fulfill(true);
	}).catch(() => {
		d.fulfill(false);
	});
	return d.promise;
}

module.exports = page;