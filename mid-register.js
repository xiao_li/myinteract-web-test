var assert = require('assert');
var webdriver = require('selenium-webdriver');
var test = require('selenium-webdriver/testing');
var Page = require('./pageobject.js');
var constants = require('./constants.js');
var driver;
var loginSuccess = true;
 
const mochaTimeOut = 30000; //ms
 
test.describe('Test Result:', function() {
    this.timeout(mochaTimeOut);
    
    test.before(function() {
        this.timeout(mochaTimeOut);
        driver = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build();
        page = new Page(driver);
        page.visit(constants.registerUrl);
    });

    test.it('can show all error messages', function() {
        page.showErrorMessage().then(function(result) {
            assert.equal(result, true, result);
        });
    });

    test.it('can view terms and policy', function() {
        page.viewTermsAndPolicy().then(function(result) {
            assert.equal(result, true, 'can\'t view ' + result);
        });
    });

    test.it('can go to login page', function() {
        page.goToLoginPage().then(function(result) {
            assert.equal(result, true, 'can\'t go to login page');
        });
    });

    test.afterEach(function() {
	    //driver.manage().deleteAllCookies();
	});
	 
	test.after(function() {
        driver.sleep(2000);
	    driver.quit();
	});
});